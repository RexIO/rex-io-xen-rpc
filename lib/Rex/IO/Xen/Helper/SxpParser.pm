#
# (c) Jan Gehring <jan.gehring@gmail.com>
# 
# vim: set ts=3 sw=3 tw=0:
# vim: set expandtab:
   
package Rex::IO::Xen::Helper::SxpParser;

use strict;
use warnings;

use Data::Dumper;

sub new {
   my $that = shift;
   my $proto = ref($that) || $that;
   my $self = { @_ };

   bless($self, $proto);

   return $self;
}

sub parse {

   my ($self, $c) = @_;

   my $data = [];
   my @d_t = split //, $c;

   my @old_data;

   my $str_builder = "";
   my $in_str = 0;
   my $in_escape = 0;

   for my $token (@d_t) {
      my @cur_data;

      if($token eq "\\" && $in_str == 1) {
         $in_escape = 1;
         next;
      }

      if($token eq "(") {
         push @{ $data }, [ @cur_data ];
         push @old_data, $data;
         $data = $data->[-1];
         $str_builder = "";
         next;
      }

      if($token eq ")") {
         push @{$data}, $str_builder if($str_builder || $str_builder eq "0");

         $data = pop @old_data;

         if(scalar @{ $data->[-1] } == 2 && ! ref($data->[-1]->[-2]) ) {
            $data->[-1] = {
               $data->[-1]->[-2],
               $data->[-1]->[-1],
            };
         }

         $str_builder = "";
         next;
      }

      if($token =~ m/^\s$/ && $str_builder && $in_str == 0) {
         push @{$data}, $str_builder;
         $str_builder = "";
         next;
      }

      if($token eq "'" && $in_escape == 0) {
         if($in_str == 0) {
            $in_str = 1;
         }
         else {
            $in_str = 0;
         }
         next;
      }

      if($token =~ m/^\s$/ && $in_str == 0) {
         next;
      }

      $str_builder .= $token;

      $in_escape = 0;
   }

   $self->make_hash($data);

   $self->{__data__} = $data->[0];
}

sub get_vnc_address {
   my ($self) = @_;

   my ($dev) = $self->get_device('vfb');

   return $dev->{location};
}

sub get_device {
   my ($self, $dev) = @_;

   my @devices = map {
      $_ = $_->{device};
   }
   
   grep {
   
      ref($_) eq "HASH"
            &&
      exists $_->{device}
            &&
      ref($_->{device}) eq "ARRAY"
            &&
      $_->{device}->[0] eq $dev
   
   } @{ $self->{__data__} };

   my @ret;

   for my $dev (@devices) {
      my %data;
      for my $entry (@{ $dev }) {
         if(ref($entry) eq "HASH") {
            my ($key) = keys %{ $entry };
            $data{$key} = $entry->{$key};
         }
      }

      push @ret, { %data };
   }

   return @ret;
}

sub make_hash {
   my ($self, $in) = @_;

   map {
      if(ref($_) eq "ARRAY" && scalar(@{ $_ }) == 1) {
         $_ = { $_->[0] => '' };
      }

      $self->make_hash($_) if(ref($_) eq "ARRAY");

      if(ref($_) eq "HASH") {
         for my $k (keys %{$_}) {
            $self->make_hash($_->{$k}) if(ref($_->{$k}) eq "ARRAY");
         }
      }
   } @{ $in };

}

1;

#
# (c) Jan Gehring <jan.gehring@gmail.com>
# 
# vim: set ts=3 sw=3 tw=0:
# vim: set expandtab:
   
package Rex::IO::Xen::Helper::Fake;

use strict;
use warnings;

require Exporter;
use base qw(Exporter);
use vars qw(@EXPORT);

use Data::Dumper;
use Mojo::Template;
use Rex::IO::Xen::Helper::SxpParser;
use IO::All;

@EXPORT = qw(list_vms create_vm start_vm stop_vm delete_vm _xm switch_vm_mode get_vnc_address list_devices get_raw_vm_config write_raw_vm_config);

# here might be pythons...
# this is code for reading xen configuration files, because they are python...
use Inline Python => <<'END_OF_PYTHON_CODE';

import imp
import re
def read_config(cfg):
   config = imp.load_source('config', cfg)
   x = filter(lambda y:not re.match(r'^__', y), config.__dict__.keys())
   ret = {}
   for key in x:
      ret[key] = config.__dict__[key]
   return ret

END_OF_PYTHON_CODE


sub start_vm {
   my ($self, $vm) = @_;
   return 1;
}

sub stop_vm {
   my ($self, $vm) = @_;
   return 1;
}

sub shutdown_vm {
   my ($self, $vm) = @_;
   return 1;
}

sub delete_vm {
   my ($self, $vm) = @_;
   return 1;
}

sub list_vms {
   my ($self) = @_;

   my @ret;

   my $path = $self->config->{xen}->{config_path};
   opendir(my $dh, $path) or die($!);
   while(my $entry = readdir($dh)) {
      next if($entry !~ m/\.cfg$/);
      push @ret, read_config("$path/$entry");
   }
   closedir($dh);

   my @vms = sort { $a->{name} cmp $b->{name} } @ret;

   @ret = ();
   for my $vm (@vms) {
      $vm->{state} = "running";
      push @ret, $vm;
   }

   return @ret;
}

sub switch_vm_mode {
   my ($self, $vm_name, $new_mode) = @_;
}

# params:
# {
#    name => "",
#    memory => 512,
#    vcpus => 1,
# }
sub create_vm {
   my ($self, $param, $options) = @_;

   $param->{memory}   ||= 512;
   $param->{vcpus}    ||= 1;
   $param->{size}     ||= 30;
   $param->{builder}  ||= "hvm";
   $param->{disk}     ||= "/mnt/data/vms/$param->{name}.img";
   $param->{dev_type} ||= "tap:qcow2";

   if(! exists $param->{mac}) {
      # create mac addresses for every given ip
      for my $ip (@{ $param->{ip} }) {
         push @{ $param->{mac} }, _gen_mac();
      }
   }

   if(! ref $param->{mac}) {
      $param->{mac} = [ $param->{mac}, _gen_mac() ];
   }

   my $t = Mojo::Template->new;
   my $template_path = $self->config->{xen}->{template_path};
   my $template_file = "$template_path/$param->{builder}.tpl.cfg";
   if(! -f $template_file) {
      $self->app->log->error("Template-File: $template_file not found.");
      die("Can't find template file.");
   }
   my $template_str = eval { local(@ARGV, $/) = ($template_file); <>; };
   my $output = $t->render($template_str, $param);

   my $config_path = $self->config->{xen}->{config_path};
   open(my $fh, ">", "$config_path/$param->{name}.cfg") or die($!);
   print $fh $output;
   close($fh);

   return $param->{mac};
}

sub get_vnc_address {
   my ($self, $vm) = @_;

   my $output = _get_sxp();

   my $sxp = Rex::IO::Xen::Helper::SxpParser->new;
   $sxp->parse($output);
   return $sxp->get_vnc_address;
}

sub list_devices {
   my ($self) = @_;

   my @out = (171, 172, 173, 174);
   chomp @out;
   map { $_ = "/dev/iscsi/$_"; } @out;

   return @out;
}

sub get_raw_vm_config {
   my ($self, $name) = @_;

   my $config_path = $self->config->{xen}->{config_path};
   return io("$config_path/$name.cfg")->slurp;
}

sub write_raw_vm_config {
   my ($self, $name, $cfg) = @_;

   my $config_path = $self->config->{xen}->{config_path};
   $cfg > io("$config_path/$name.write.cfg");
}

################################################################################
# private functions
################################################################################

sub _gen_mac {

   my $mac = "00:16:3e:";

   for (my $i=0;$i<3;$i++) {
      $mac.=sprintf("%02x",int(rand(255))).(($i<2)?':':'');
   }

   return $mac;

}

sub _parse_xm_list {
   my ($input) = @_;

   my @lines = split(/\n/, $input);
   shift @lines;
   shift @lines;

   my $ret = {};
   for my $line (@lines) {
      chomp $line;
      my ($name, $id, $mem, $vcpus, $state, $time) = split(/\s+/, $line);
      $ret->{$name} = {
         name    => $name,
         id      => $id,
         mem     => $mem,
         vcpus   => $vcpus,
         state   => _parse_state($state),
         time    => $time,
      };
   }

   return $ret;
}

sub _parse_state {
   my ($state) = @_;

   if($state =~ m/^r\-/) {
      return "running";
   }

   if($state =~ m/^\-b/) {
      return "running";
   }

   return "unknown";
}

sub _xm {
   my ($self, @opts) = @_;
   my $exec = $self->config->{xen}->{xm};
   if(@opts) {
      $exec .= " " . join(" ", @opts);
   }

   return "";
}


sub _get_sxp {

   return <<EOF
(domain
    (domid 379)
    (cpu_weight 256)
    (cpu_cap 0)
    (pool_name Pool-0)
    (bootloader '')
    (vcpus 2)
    (cpus ((8 9 10 11 12 13 14 15) (8 9 10 11 12 13 14 15)))
    (on_poweroff destroy)
    (on_crash restart)
    (uuid 8682f8d3-169f-ff42-26ff-dbdb52642596)
    (bootloader_args '')
    (name app-demo2)
    (on_reboot restart)
    (maxmem 1028)
    (memory 1023)
    (shadow_memory 10)
    (features '')
    (on_xend_start ignore)
    (on_xend_stop ignore)
    (start_time 1382017531.43)
    (cpu_time 2865.89638646)
    (online_vcpus 2)
    (image
        (hvm
            (kernel '')
            (superpages 0)
            (videoram 4)
            (hpet 0)
            (stdvga 0)
            (vnclisten 0.0.0.0)
            (loader /usr/lib/xen/boot/hvmloader)
            (xen_platform_pci 1)
            (rtc_timeoffset 0)
            (pci ())
            (hap 1)
            (localtime 0)
            (xenpaging_extra ())
            (actmem 0)
            (pci_msitranslate 1)
            (oos 1)
            (apic 0)
            (xenpaging_file '')
            (timer_mode 1)
            (vpt_align 1)
            (serial pty)
            (vncunused 1)
            (boot nc)
            (pae 1)
            (viridian 0)
            (acpi 0)
            (vnc 1)
            (nographic 0)
            (watchdog_action reset)
            (nomigrate 0)
            (usb 0)
            (tsc_mode 0)
            (guest_os_type default)
            (device_model /usr/lib64/xen/bin/qemu-dm)
            (pci_power_mgmt 0)
            (xauthority /root/.Xauthority)
            (isa 0)
            (notes (SUSPEND_CANCEL 1))
        )
    )
    (status 2)
    (state -b----)
    (device
        (vif
            (bridge br0)
            (mac 00:16:3e:fd:b1:21)
            (script /etc/xen/scripts/vif-bridge)
            (uuid ffd49581-e5e2-4c39-c9b9-15f9b66044a2)
            (backend 0)
        )
    )
    (device
        (vif
            (bridge br1)
            (mac 00:16:3e:ee:94:f1)
            (script /etc/xen/scripts/vif-bridge)
            (uuid 1f7e5880-be8c-65b8-1bd1-88f27c97b1b5)
            (backend 0)
        )
    )
    (device
        (console
            (protocol vt100)
            (location 4)
            (uuid dafdc791-647b-2407-b968-bfa209676ab8)
        )
    )
    (device
        (vbd
            (protocol x86_64-abi)
            (uuid 27f0c899-067e-a4c6-4e4e-50b6773090e7)
            (bootable 1)
            (dev xvda:disk)
            (uname phy:/dev/iscsi/173)
            (mode w)
            (backend 0)
            (VDI 'foo \'bar\'')
        )
    )
    (device
        (vfb
            (vncunused 1)
            (vnclisten 0.0.0.0)
            (vnc 1)
            (uuid cf6cc24a-220e-cdd8-1dce-b55ff24df789)
            (location 0.0.0.0:5901)
        )
    )
)


EOF

}

1;
__END__


   print $fh qq~# hvm needed for pxe boot
builder = 'hvm'
#kernel = "/boot/vmlinuz-3.0.13-0.27-xen"
#ramdisk = "/boot/initrd-3.0.13-0.27-xen"
kernel = "/usr/lib/xen/boot/hvmloader"
device_model = "/usr/lib64/xen/bin/qemu-dm"
vnc = 1
vncunused = 1
vnclisten = '0.0.0.0'
apic = 0
acpi = 0
pae = 1
boot = 'nc'
serial = 'pty'
name = "$param->{name}"
memory = "$param->{memory}"
disk = [ "tap:qcow2:/mnt/data/vms/$param->{name}.img,xvda,w" ]
vif = [ "mac=$param->{mac},bridge=br0" ]
vcpus = $param->{vcpus}
on_reboot = "restart"
on_crash = "restart"
~;


#
# (c) Jan Gehring <jan.gehring@gmail.com>
# 
# vim: set ts=3 sw=3 tw=0:
# vim: set expandtab:
   
package Rex::IO::Xen::Helper::Xen;

use strict;
use warnings;

require Exporter;
use base qw(Exporter);
use vars qw(@EXPORT);

use Mojo::Template;
use Rex::IO::Xen::Helper::SxpParser;
use File::Basename;
use IO::All;

@EXPORT = qw(list_vms create_vm start_vm stop_vm delete_vm _xm switch_vm_mode get_vnc_address list_devices get_raw_vm_config write_raw_vm_config _get_cfg_file);


# here might be pythons...
# this is code for reading xen configuration files, because they are python...
use Inline Python => <<'END_OF_PYTHON_CODE';

import imp
import re
def read_config(cfg):
   config = imp.load_source('config', cfg)
   x = filter(lambda y:not re.match(r'^__', y), config.__dict__.keys())
   ret = {}
   for key in x:
      ret[key] = config.__dict__[key]
   return ret

END_OF_PYTHON_CODE


sub start_vm {
   my ($self, $vm) = @_;

   my $xm;
   if($self->config->{xen}->{persistent_vms}) {
      $xm = $self->_xm(start => $vm);
   }
   else {
      $xm = $self->_xm(create => $self->_get_cfg_file("$vm"));
   }

   if($? != 0) { die("Error starting vm: $vm"); }

   return 1;
}

sub stop_vm {
   my ($self, $vm) = @_;
   my $xm = $self->_xm(destroy => $vm);
   if($? != 0) { die("Error stopping vm: $vm"); }

   return 1;
}

sub shutdown_vm {
   my ($self, $vm) = @_;
   my $xm = $self->_xm(shutdown => $vm);
   if($? != 0) { die("Error shutdown vm: $vm"); }

   return 1;
}

sub delete_vm {
   my ($self, $vm) = @_;

   $self->_xm(destroy => $vm);

   if(exists $self->config->{xen}->{dump_configuration_before_delete}
      && $self->config->{xen}->{dump_configuration_before_delete} == 1) {
      $self->_xm("list", "-l", $vm, ">" . $self->config->{xen}->{dump_configuration_path} . "/$vm.sxp");
   }

   if($self->config->{xen}->{persistent_vms}) {
      $self->_xm(delete => $vm);
   }

   if(exists $self->config->{xen}->{remove_configuration_file} && $self->config->{xen}->{remove_configuration_file} == 1) {
      if(exists $self->config->{xen}->{report_only} && $self->config->{xen}->{report_only} == 1) {
         $self->app->log->info("Removing configuration file: " . $self->_get_cfg_file($vm));
      }
      else {
         unlink $self->_get_cfg_file($vm);
      }
   }

   if(exists $self->config->{xen}->{remove_data_file} && $self->config->{xen}->{remove_data_file} == 1) {
      if(exists $self->config->{xen}->{report_only} && $self->config->{xen}->{report_only} == 1) {
         $self->app->log->info("Removing data file: /mnt/data/vms/$vm.img");
      }
      else {
         if(-f "/mnt/data/vms/$vm.img") {
            unlink "/mnt/data/vms/$vm.img";
         }
      }
   }

   return 1;
}

sub list_vms {
   my ($self) = @_;

   my @ret;

   my $xm_list_out = $self->_xm("list");
   my $xm_list     = _parse_xm_list($xm_list_out);

   my $path = $self->config->{xen}->{config_path};
   opendir(my $dh, $path) or die($!);
   while(my $entry = readdir($dh)) {
      next if($entry =~ m/^\./);
      eval { push @ret, read_config("$path/$entry"); };
   }
   closedir($dh);

   my @vms = sort { $a->{name} cmp $b->{name} } @ret;

   @ret = ();
   for my $vm (@vms) {
      if(! exists $xm_list->{$vm->{name}} ) {
         $vm->{state} = "stopped";  
      }
      else {
         $vm->{state} = $xm_list->{$vm->{name}}->{state};
      }
      push @ret, $vm;
   }

   return @ret;
}

sub switch_vm_mode {
   my ($self, $vm_name, $new_mode) = @_;

   $new_mode //= "hvm";

   if(! -f $self->_get_cfg_file($vm_name)) {
      die("Can't find configuration file.");
   }

   $self->app->log->info("Switching VM mode to $new_mode");
   my $vm_conf_file = $self->_get_cfg_file($vm_name);

   if(exists $self->config->{xen}->{report_only} && $self->config->{xen}->{report_only} == 1) {
      $vm_conf_file = $self->config->{xen}->{report_tmp_config_path} . "/$vm_name";
   }

   my @vm_config_arr = eval { local(@ARGV) = ($self->_get_cfg_file($vm_name)); <>;};
   chomp @vm_config_arr;
   my @new_conf;

   #my $vm = _parse_vm_config($vm_config_str);

   if($new_mode eq "pvm") {
   
      for my $line (@vm_config_arr) {
         next if($line =~ m/^builder/);
         next if($line =~ m/^kernel/);
         next if($line =~ m/^ramdisk/);

         push @new_conf, $line;
      }

      #push @new_conf, 'kernel = "' . $self->config->{xen}->{pvm}->{kernel} . '"';
      #push @new_conf, 'ramdisk = "' . $self->config->{xen}->{pvm}->{ramdisk} . '"';
      push @new_conf, 'builder = "linux"';
      push @new_conf, 'bootloader="/usr/bin/pygrub"';
      push @new_conf, 'bootargs=""';
   }
   elsif($new_mode eq "hvm") {

      for my $line (@vm_config_arr) {
         next if($line =~ m/^builder/);
         next if($line =~ m/^kernel/);
         next if($line =~ m/^ramdisk/);
         next if($line =~ m/^bootloader/);
         next if($line =~ m/^bootargs/);

         push @new_conf, $line;
      }

      push @new_conf, 'builder = "hvm"';
      push @new_conf, 'kernel = "/usr/lib/xen/boot/hvmloader"';
   }

   open(my $fh, ">", $vm_conf_file) or die($!);
   print $fh join("\n", @new_conf);
   close($fh);

   if($self->config->{xen}->{persistent_vms}) {
      $self->_xm(delete => $vm_name);
      if($? != 0) {
         die("Can't delete $vm_name. Do you need to stop it?");
      }
      $self->_xm(new => $vm_conf_file);
      if($? != 0) {
         die("Can't create new $vm_name. Please try to create it manually.");
      }
   }
}

# params:
# {
#    name => "",
#    memory => 512,
#    vcpus => 1,
# }
sub create_vm {
   my ($self, $param) = @_;

   $param->{memory}   ||= 512;
   $param->{vcpus}    ||= 1;
   $param->{size}     ||= 30;
   $param->{builder}  ||= "hvm";
   $param->{disk}     ||= "/mnt/data/vms/$param->{name}.img";
   $param->{dev_type} ||= "tap:qcow2";

   if(! exists $param->{mac}) {
      # create mac addresses for every given ip
      for my $ip (@{ $param->{ip} }) {
         push @{ $param->{mac} }, _gen_mac();
      }
   }

   if(! ref $param->{mac}) {
      $param->{mac} = [ $param->{mac}, _gen_mac() ];
   }

   my $img_cmd = $self->config->{xen}->{img};
   if($param->{disk} !~ m/^\/dev/) {
      # this is a local file image
      if(! -f $param->{disk}) {

         if(exists $self->config->{xen}->{report_only} && $self->config->{xen}->{report_only} == 1) {
            $self->app->log->info("(report) $img_cmd create -f qcow2 $param->{disk} $param->{size}G");
         }
         else {
            $self->app->log->info("(executing) $img_cmd create -f qcow2 $param->{disk} $param->{size}G");
            system("$img_cmd create -f qcow2 $param->{disk} $param->{size}G");
         }

         $param->{dev_type} = "tap:qcow2";
      }
   }
   elsif($param->{disk} =~ m/^\/dev/) {
      # this is a physical device
      $param->{dev_type} = "phy";
   }

   $self->app->log->info("(create_vm) dev_type: $param->{dev_type}");

   my $t = Mojo::Template->new;
   my $template_path = $self->config->{xen}->{template_path};
   my $template_file = "$template_path/$param->{builder}.tpl.cfg";
   if(! -f $template_file) {
      $self->app->log->error("Template-File: $template_file not found.");
      die("Can't find template file.");
   }
   my $template_str = eval { local(@ARGV, $/) = ($template_file); <>; };
   my $output = $t->render($template_str, $param);

   my $vm_conf_file = $self->config->{xen}->{config_path} . "/$param->{name}";
   if(exists $self->config->{xen}->{report_only} && $self->config->{xen}->{report_only} == 1) {
      $vm_conf_file = $self->config->{xen}->{report_tmp_config_path} . "/$param->{name}";
   }

   open(my $fh, ">", $vm_conf_file) or die($!);
   print $fh $output;
   close($fh);

   if($self->config->{xen}->{persistent_vms}) {
      $self->_xm(new => $vm_conf_file);
      $self->_xm(start => $param->{name});
   }
   else {
      $self->_xm(create => $vm_conf_file);
   }

   if($? != 0) {
      die("Can't start VM: $param->{name}");
   }

   return $param->{mac};
}

sub get_vnc_address {
   my ($self, $vm) = @_;

   my $output = $self->_xm("list", $vm, "-l");

   my $sxp = Rex::IO::Xen::Helper::SxpParser->new;
   $sxp->parse($output);
   return $sxp->get_vnc_address;
}

sub list_devices {
   my ($self) = @_;

   my $path = $self->config->{xen}->{phy_device_glob};
   my $base_path = dirname($path);

   my @out = qx{ls -1 $path};
   chomp @out;

   return @out;
}

sub get_raw_vm_config {
   my ($self, $name) = @_;

   $self->app->log->info("Getting vm configuration of $name");
   my $config_path = $self->_get_cfg_file($name);
   return io($config_path)->slurp;
}

sub write_raw_vm_config {
   my ($self, $name, $cfg) = @_;

   $self->app->log->info("Writing vm configuration of $name");
   my $config_path = $self->config->{xen}->{config_path} . "/$name";
   $cfg > io($config_path);

   if($self->config->{xen}->{persistent_vms}) {
      $self->_xm(new => $config_path);
   }

   if($? != 0) {
      die("Can't create VM: $name");
   }
}

################################################################################
# private functions
################################################################################

sub _get_cfg_file {
   my ($self, $vm) = @_;

   my $cfg_path = $self->config->{xen}->{config_path} . "/$vm";
   if(-f $cfg_path) {
      return $cfg_path;
   }

   elsif(-f "$cfg_path.cfg") {
      return "$cfg_path.cfg";
   }

   else {
      die("error, can't find configuration file for vm: $vm.");
   }
}

sub _parse_vm_config {
   my ($output) = @_;

   my @lines = split(/\n/, $output);

   my $ret = {};
   for my $line (@lines) {
      chomp $line;

      next if($line =~ m/^$/);
      next if($line =~ m/^#/);

      my ($key, $val) = split(/=/, $line, 2);
      $key =~ s/['"\s]//g;
      $val =~ s/['"\s]//g;

      $ret->{$key} = $val;
   }

   return $ret;
}

sub _gen_mac {

   my $mac = "00:16:3e:";

   for (my $i=0;$i<3;$i++) {
      $mac.=sprintf("%02x",int(rand(255))).(($i<2)?':':'');
   }

   return $mac;

}

sub _parse_xm_list {
   my ($input) = @_;

   my @lines = split(/\n/, $input);
   shift @lines;

   my $ret = {};
   for my $line (@lines) {
      chomp $line;
      my ($name, $id, $mem, $vcpus, $state, $time) = split(/\s+/, $line);
      $ret->{$name} = {
         name    => $name,
         id      => $id,
         mem     => $mem,
         vcpus   => $vcpus,
         state   => _parse_state($state),
         time    => $time,
      };
   }

   return $ret;
}

sub _parse_state {
   my ($state) = @_;

   if($state =~ m/^r\-/) {
      return "running";
   }

   if($state =~ m/^\-b/) {
      return "running";
   }

   if(! $state) {
      return "stopped";
   }

   return "unknown";
}

sub _xm {
   my ($self, @opts) = @_;
   my $exec = $self->config->{xen}->{xm};
   if(@opts) {
      $exec .= " " . join(" ", @opts);
   }

   if(exists $self->config->{xen}->{report_only}
      && $self->config->{xen}->{report_only} == 1
      && $opts[0] ne "list") {
      $self->app->log->info("Executing (report_only): $exec");
   }
   else {
      $self->app->log->info("Executing: $exec");
      return qx{$exec};
   }
}


1;
__END__


   print $fh qq~# hvm needed for pxe boot
builder = 'hvm'
#kernel = "/boot/vmlinuz-3.0.13-0.27-xen"
#ramdisk = "/boot/initrd-3.0.13-0.27-xen"
kernel = "/usr/lib/xen/boot/hvmloader"
device_model = "/usr/lib64/xen/bin/qemu-dm"
vnc = 1
vncunused = 1
vnclisten = '0.0.0.0'
apic = 0
acpi = 0
pae = 1
boot = 'nc'
serial = 'pty'
name = "$param->{name}"
memory = "$param->{memory}"
disk = [ "tap:qcow2:/mnt/data/vms/$param->{name}.img,xvda,w" ]
vif = [ "mac=$param->{mac},bridge=br0" ]
vcpus = $param->{vcpus}
on_reboot = "restart"
on_crash = "restart"
~;


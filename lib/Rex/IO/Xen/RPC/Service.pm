package Rex::IO::Xen::RPC::Service;
use Mojo::Base 'Mojolicious::Controller';

use Rex::IO::Xen::Helper::Xen;
use Mojo::JSON;

sub index {
   my $self = shift;

   # Render template "example/welcome.html.ep" with message
   $self->render( text => 'Rex::IO::Xen::RPC Version ' . $Rex::IO::Xen::RPC::VERSION );
}

sub ctrl_switch_vm_mode {
   my $self = shift;

   eval {
      $self->switch_vm_mode($self->param("name"), $self->param("type"));
      1;
   } or do {
      return $self->render(json => { ok => Mojo::JSON->false, error => "Error switching virtualization mode.", desc => $@}, status => 500);
   };

   $self->render(json => { ok => Mojo::JSON->true });
}

sub list_phy_devices {
   my $self = shift;
   return $self->render(json => {ok => Mojo::JSON->true, data => [ $self->list_devices() ]});
}

sub ctrl_stop_vm {
   my $self = shift;

   my $vm = $self->param("name");
   
   eval {
      $self->stop_vm($vm);
      return 1;
   } or do {
      return $self->render(json => { ok => Mojo::JSON->false, error => $@ }, status => 500);
   };

   $self->render(json => { ok => Mojo::JSON->true } );
}

sub ctrl_shutdown_vm {
   my $self = shift;

   my $vm = $self->param("name");
   
   eval {
      $self->shutdown_vm($vm);
      return 1;
   } or do {
      return $self->render(json => { ok => Mojo::JSON->false, error => $@ }, status => 500);
   };

   $self->render(json => { ok => Mojo::JSON->true } );
}

sub ctrl_delete_vm {
   my $self = shift;

   my $vm = $self->param("name");
   
   eval {
      $self->delete_vm($vm);
      return 1;
   } or do {
      return $self->render(json => { ok => Mojo::JSON->false, error => $@ }, status => 500);
   };

   $self->render(json => { ok => Mojo::JSON->true } );
}

sub ctrl_start_vm {
   my $self = shift;

   my $vm = $self->param("name");
   
   eval {
      $self->start_vm($vm);
      return 1;
   } or do {
      return $self->render(json => { ok => Mojo::JSON->false, error => $@ }, status => 500);
   };

   $self->render(json => { ok => Mojo::JSON->true } );
}

sub ctrl_list_vms {
   my $self = shift;

   my @vms;

   eval {
      @vms = $self->list_vms;
      1;
   } or do {
      return $self->render(json => {ok => Mojo::JSON->false, error => $@}, status => 500);
   };

   return $self->render(json => {ok => Mojo::JSON->true, data => \@vms});
}

sub ctrl_create_vm {
   my $self = shift;

   my $json = $self->req->json;
   my $name = $self->param("name");
   $json->{name} = $name;

   my $mac;
   eval {
      $mac = $self->create_vm($json);
      return 1;
   } or do {
      return $self->render(json => {ok => Mojo::JSON->false, error => $@}, status => 500);
   };

   return $self->render(json => {ok => Mojo::JSON->true, mac => $mac});
}

sub ctrl_get_vnc_address {
   my ($self) = @_;

   my $vnc = $self->get_vnc_address($self->param("name"));

   if(! $vnc) {
      return $self->render(json => {ok => Mojo::JSON->true, error => "no vnc found"}, status => 404);
   }

   $self->render(json => {ok => Mojo::JSON->true, vnc => $vnc});
}

sub ctrl_write_vm_config {
   my ($self) = @_;

   eval {
      $self->write_raw_vm_config($self->param("name"), $self->req->json->{data});
      1;
   } or do {
      $self->app->log->error("Error writing configuration file. Please check permissions.");
      return $self->render(json => { ok => Mojo::JSON->false, error => "Error writing configuration file. Please check permissions." });
   };

   $self->render(json => { ok => Mojo::JSON->true });
}

sub ctrl_get_vm_config {
   my ($self) = @_;

   my $data = $self->get_raw_vm_config($self->param("name"));

   $self->render(json => { data => $data });
}


1;

package Rex::IO::Xen::RPC;
use Mojo::Base 'Mojolicious';
use Cwd 'getcwd';

our $VERSION = "0.2.42";

# This method will run once at server start
sub startup {
   my $self = shift;

   #######################################################################
   # Load configuration
   #######################################################################
   my @cfg = ("/etc/rex/io/xen_rpc.conf", "/usr/local/etc/rex/io/xen_rpc.conf", getcwd() . "/xen_rpc.conf");
   my $cfg;
   for my $file (@cfg) {
      if(-f $file) {
         $cfg = $file;
         last;
      }
   }
 
   #######################################################################
   # Load plugins
   #######################################################################
   $self->plugin("Config", file => $cfg);
 
   #######################################################################
   # Routing
   #######################################################################

   $self->app->routes->add_condition(authenticated => sub {
      my ($r, $c, $captures, $required) = @_;
      if(exists $self->config->{accept_connection_from}) {
         if($c->tx->remote_address eq $self->config->{accept_connection_from}) {
            return 1;
         }

         return 0;
      }
      else {
         return 1;
      }
   });

   my $r = $self->routes;

   # Normal route to controller
   $r->get("/")->over(authenticated => 1)->to("service#index");
   $r->get("/vms")->over(authenticated => 1)->to("service#ctrl_list_vms");

   $r->get("/vm/*name/config")->over(authenticated => 1)->to("service#ctrl_get_vm_config");
   $r->get("/vm/*name/vnc")->over(authenticated => 1)->to("service#ctrl_get_vnc_address");
   $r->post("/vm/*name/start")->over(authenticated => 1)->to("service#ctrl_start_vm");
   $r->post("/vm/*name/stop")->over(authenticated => 1)->to("service#ctrl_stop_vm");
   $r->post("/vm/*name/delete")->over(authenticated => 1)->to("service#ctrl_delete_vm");
   $r->post("/vm/*name/shutdown")->over(authenticated => 1)->to("service#ctrl_shutdown_vm");
   $r->post("/vm/*name/config")->over(authenticated => 1)->to("service#ctrl_write_vm_config");
   $r->get("/devices")->over(authenticated => 1)->to("service#list_phy_devices");
   $r->post("/vm/*name/mode/:type")->over(authenticated => 1)->to("service#ctrl_switch_vm_mode");
   $r->post("/vm/*name")->over(authenticated => 1)->to("service#ctrl_create_vm");
}

1;

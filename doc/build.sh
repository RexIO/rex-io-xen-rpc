#!/bin/sh

VER=$1

carton install

mkdir rex-io-xen-rpc-$VER
mkdir rex-io-xen-rpc-$VER/doc
cp -R {bin,lib,t,local} rex-io-xen-rpc-$VER
cp doc/rex-io-xen-rpc.init rex-io-xen-rpc-$VER/doc

tar czf rex-io-xen-rpc-$VER.tar.gz rex-io-xen-rpc-$VER

rm -rf rex-io-xen-rpc-$VER

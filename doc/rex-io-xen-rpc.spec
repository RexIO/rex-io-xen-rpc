Summary: Rex.IO - Xen RPC
Name: rex-io-xen-rpc
Version: 0.2.42
Release: 2
License: Apache 2.0
Group: Utilities/System
Source: http://rex.io/downloads/rex-io-xen-rpc-0.2.42.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root
AutoReqProv: no

BuildRequires: rexio-perl >= 5.18.0
BuildRequires: rexio-perl-Inline-Python
#Requires: libssh2 >= 1.2.8 - is included in perl-Net-SSH2 deps
Requires: rexio-perl >= 5.18.0
Requires: rexio-perl-Inline-Python
Requires: rexio-perl-IO-All

%description
Rex.IO Xen RPC - RPC Service to control Xen

%prep
%setup -n %{name}-%{version}


%install
%{__rm} -rf %{buildroot}
%{__mkdir} -p %{buildroot}/srv/rexio/xen_rpc
%{__mkdir} -p %{buildroot}/etc/init.d
%{__cp} -R {bin,lib,t,local} %{buildroot}/srv/rexio/xen_rpc
%{__cp} doc/rex-io-xen-rpc.init %{buildroot}/etc/init.d/rex-io-xen-rpc
%{__chmod} 755 %{buildroot}/etc/init.d/rex-io-xen-rpc

### Clean up buildroot
find %{buildroot} -name .packlist -exec %{__rm} {} \;

%pre

# create rex.io user if not there
if ! id rexio &>/dev/null; then
	groupadd -r rexio &>/dev/null
	useradd -r -d /srv/rexio -c 'Rex.IO Service User' -g rexio -m rexio &>/dev/null
fi

%post

/bin/chown -R rexio. /srv/rexio

if [ ! -d /etc/rex/io ]; then
   mkdir -p /etx/rex/io
fi

if [ ! -f /etc/rex/io/xen_rpc.conf ]; then

if [ -d /dev/iscsi ]; then

cat >/etc/rex/io/xen_rpc.conf<<EOF

{

   hypnotoad => {
      listen => ["http://*:4000?"],
      pid_file => "/var/run/rex_io_xen_rpc.pid",
   },

   xen => {
      xm => "sudo /usr/sbin/xm",
      img => "sudo /usr/bin/qemu-img",
      pvm => {
         kernel  => "/boot/vmlinuz-xen",
         ramdisk => "/boot/initrd-xen",
      },
      config_path => "/etc/xen/vm",
      template_path => "/etc/rex/io/xen",
      phy_device_glob => "/dev/iscsi/*",
      persistent_vms => 1,

      report_only => 1,
      report_tmp_config_path => "/tmp",
      dump_configuration_path => "/tmp",
      dump_configuration_before_delete => 1,
      remove_configuration_file => 0,
      remove_data_file => 0,

   },

};

EOF



else

cat >/etc/rex/io/xen_rpc.conf<<EOF

{

   hypnotoad => {
      listen => ["http://*:4000?"],
      pid_file => "/var/run/rex_io_xen_rpc.pid",
   },

   xen => {
      xm => "sudo /usr/sbin/xm",
      img => "sudo /usr/bin/qemu-img",
      pvm => {
         kernel  => "/boot/vmlinuz-xen",
         ramdisk => "/boot/initrd-xen",
      },
      config_path => "/etc/xen/vm",
      template_path => "/etc/rex/io/xen",
      phy_device_glob => "/dev/disk/by-path/*netapp*-lun-* | grep -v part",
      persistent_vms => 1,

      report_only => 1,
      report_tmp_config_path => "/tmp",
      dump_configuration_path => "/tmp",
      dump_configuration_before_delete => 1,
      remove_configuration_file => 0,
      remove_data_file => 0,

   },

};

EOF

fi

# add sudoers entry
grep -q ^rexio /etc/sudoers
if [ $? != 0 ]; then 
   echo "" >>/etc/sudoers
   echo "rexio ALL = NOPASSWD: /usr/sbin/xm, /usr/bin/qemu-img" >>/etc/sudoers
fi

fi

# set fs acls
setfacl -dm u:rexio:rwx /etc/xen/vm
setfacl -m u:rexio:rwx /etc/xen/vm
chmod 755 /etc/xen

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root, 0755)
/srv/rexio/xen_rpc
/etc/init.d/rex-io-xen-rpc

%changelog

* Thu Jul 25 2013 Jan Gehring <jan.gehring at, gmail.com> 0.2.42-1
- initial packaged


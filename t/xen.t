use Test::More;
use Data::Dumper;

use_ok 'Rex::IO::Xen::Helper::Xen';

Rex::IO::Xen::Helper::Xen->import();

my $xm_list_out = eval { local(@ARGV, $/) = ("t/xm_list.txt"); <>; };
my $vm_config   = eval { local(@ARGV, $/) = ("t/vms/test1.cfg"); <>; };


my $vm = Rex::IO::Xen::Helper::Xen::_parse_vm_config($vm_config);
my $xm_list = Rex::IO::Xen::Helper::Xen::_parse_xm_list($xm_list_out);

ok($vm->{name} eq "test1", "got test1 vm");
ok($vm->{acpi} == 1, "got test1 vm - acpi");
ok($vm->{vif} eq "[type=ioemu,mac=00:16:3e:35:d8:37,bridge=br0]", "got test1 vm - vif");
ok($xm_list->{test02}->{id} == 14, "xm_list got id");
ok($xm_list->{test01}->{id} == 7, "xm_list got id");
ok($xm_list->{test01}->{state} eq "running", "xm_list got state running");
ok($xm_list->{test02}->{state} eq "running", "xm_list got state running");
ok($xm_list->{test03}->{state} eq "unknown", "xm_list got state unknown");

ok(Rex::IO::Xen::Helper::Xen::_gen_mac() =~ m/00:16:3e:..:..:../, "got mac address");

done_testing();

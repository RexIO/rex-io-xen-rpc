use Mojo::Base -strict;

use Test::More;
use Test::Mojo;

use_ok 'Rex::IO::Xen::Helper::Xen';
use_ok 'Rex::IO::Xen::RPC';
use_ok 'Rex::IO::Xen::RPC::Service';

my $t = Test::Mojo->new('Rex::IO::Xen::RPC');
$t->get_ok('/')->status_is(200)->content_like(qr/Version/i);


done_testing();
